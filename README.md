# pixxon-demo-exe

```bash
buckaroo install
mkdir build
cd build
cmake ..
make
./demo
```

When you want to upgrade the library:

```bash
buckaroo upgrade
```
