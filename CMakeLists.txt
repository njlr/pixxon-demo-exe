cmake_minimum_required(VERSION 3.12)

project(demo)

add_subdirectory(buckaroo/gitlab/njlr/pixxon-demo-lib)

add_executable(demo src/main.cpp)

target_link_libraries(demo hello)
